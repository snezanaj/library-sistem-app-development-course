package test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;


public class ORMTestClientUpdated {

  public static void main(String[] args) throws IOException {

    ORMClient ormClient1 = new ORMClient();
    ORMClient ormClient2 = new ORMClient();
    ORMClient ormClient3 = new ORMClient();
    ORMClient ormClient4 = new ORMClient();
    ORMClient ormClient5 = new ORMClient();
    ORMClient ormClient6 = new ORMClient();

    ormClient1.start();
    ormClient2.start();
    ormClient3.start();
    ormClient4.start();
    ormClient5.start();
    ormClient6.start();
  }

  static class ORMClient extends Thread {
    static int ASSIGNMENT_NUM = 1;
    final int number;
    final Socket socket = new Socket(InetAddress.getByName("localhost"), 8080);
    final BufferedReader br = new BufferedReader(new InputStreamReader(socket.getInputStream()));
    final PrintWriter pw = new PrintWriter(socket.getOutputStream());

    ORMClient() throws IOException {
      number = ASSIGNMENT_NUM++;
      this.setName("Client " + number);
    }

    @Override
    public void run() throws UnsupportedOperationException {
      if (number < 1 || number > 6) {
        throw new UnsupportedOperationException("Question number not supported.");
      }
      try {
        sendClientMessageToServer();
        receiveResponseFromServer();
      } catch (Exception e) {
        e.printStackTrace();
      } finally {
        pw.close();
        try {
          br.close();
        } catch (IOException e) {
          e.printStackTrace();
        }
      }
    }

    private void sendClientMessageToServer() {
      String message = currentThread().getName() + " with QUESTION " + number + "\r\n";
      pw.println("POST /ORM-Lib HTTP/1.1");
      pw.println("Host: localhost");
      pw.println();
      pw.print(message);
      pw.flush();
      System.out.println("Client " + number + " sent message:\r\n\r\n" + message + "\r\n");
    }

    private void receiveResponseFromServer() throws IOException {
      StringBuilder responseLines = new StringBuilder();
      responseLines.append("\r\n");
      String response;
      while ((response = br.readLine()) != null) {
        responseLines.append(response);
        responseLines.append("\r\n");
      }
      System.out.println("Client " + number + " got response:\r\n" + responseLines.toString());
    }
  }
}