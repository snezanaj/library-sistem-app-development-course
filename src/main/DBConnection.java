package main;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class DBConnection {

  public static String askQuestion(int questionNumber) {
    ArrayList<String> returnStrings = new ArrayList<>();
    String jdbcPath = "jdbc:sqlite:project-library.db";

    String sql = switch (questionNumber) {

      //Q1 Insert a new book information
      case 1 -> "INSERT INTO books (title, publisher_id)\n" +
          "VALUES ('Don Quixote and Sancho Panza', 7)";

      //Q2 Update a user name and password
      case 2 -> "UPDATE users SET name = 'Super Nina', password ='Super Secret 123456' WHERE name = 'Nina'";

      //Q3 Delete a loan record
      case 3 -> "DELETE FROM book_loans WHERE id='8'";

      //Q4 Retrieve the names of all borrowers who borrowed the book titled "A" for each library branch.
      case 4 -> "SELECT name FROM borrowers br\n" +
          "INNER JOIN book_loans bl ON bl.borrower_id = br.id AND bl.borrower_id = br.id\n" +
          "INNER JOIN book_copies bc ON bc.id = bl.book_copy_id\n" +
          "INNER JOIN books b ON bc.book_id = b.id\n" +
          "WHERE b.title = 'Catcher in the Rye'" +
          "GROUP BY br.id";

      //Q5 For each book that is loaned out from the branch "A" and whose due date is today, retrieve the book
      //title, the borrower's name(s), and the borrower's address(es).
      case 5 -> "SELECT title, borrowers.name, borrowers.address \n" +
          "FROM book_copies\n" +
          "INNER JOIN book_loans ON book_loans.book_copy_id = book_copies.id AND book_loans.due_date=CURRENT_DATE\n" +
          "INNER JOIN branches ON branches.name='Architecture and Civil Engineering Library'\n" +
          "INNER JOIN borrowers ON borrowers.id=book_loans.borrower_id\n" +
          "INNER JOIN books ON books.id= book_copies.book_id";

      //Q6 For each branch, retrieve the branch name and the total number of books loaned out from that branch
      case 6 -> "SELECT branches.name as LibraryBranch, COUNT(*) as NumberOfBooksBorrowed \n" +
          "FROM book_copies\n" +
          "INNER JOIN book_loans ON book_loans.book_copy_id = book_copies.id\n" +
          "INNER JOIN branches ON branches.id=book_copies.branch_id\n" +
          "GROUP BY branches.name";
      default -> throw new IllegalStateException("Unexpected value: " + questionNumber);
    };

    try {
      Connection connection = DriverManager.getConnection(jdbcPath);
      Statement statement = connection.createStatement();
      if (questionNumber < 4) {
        switch (questionNumber) {
          case 1 -> {
            statement.executeUpdate(sql);
            returnStrings.add("Database insertion complete.");
          }
          case 2 -> {
            statement.executeUpdate(sql);
            returnStrings.add("Database update complete.");
          }
          case 3 -> {
            statement.executeUpdate(sql);
            returnStrings.add("Database deletion complete.");
          }
        }
      } else {
        ResultSet result = statement.executeQuery(sql);
        while (result.next()) {
          switch (questionNumber) {
            case 4 -> returnStrings.add(result.getString("name"));
            case 5 -> {
              String title = result.getString("title");
              String borrowerName = result.getString("name");
              String address = result.getString("address");
              returnStrings.add(title + ": " + borrowerName + " - " + address);
            }
            case 6 -> {
              String branch = result.getString("LibraryBranch");
              String booksBorrowed = result.getString("NumberOfBooksBorrowed");
              returnStrings.add(branch + " - number of currently borrowed books: " + booksBorrowed);
            }
          }
        }
      }
    } catch (SQLException e) {
      System.out.println("Error when connecting to the SQLite database.");
      e.printStackTrace();
    }
    return String.join("\r\n", returnStrings);
  }
}
