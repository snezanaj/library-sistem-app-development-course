package main;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Date;

// Server class
public class HTTPServerMultithreaded {
  //server port definition.
  static final int PORT = 8080;

  public static void main(String[] args) {
    ServerSocket server = null;

    try {
      server = new ServerSocket(PORT);
      server.setReuseAddress(true);

      // running infinite loop for getting client request
      //noinspection InfiniteLoopStatement
      while (true) {

        // socket object to receive incoming client requests
        Socket connection = server.accept();
        new ClientHandler(connection).start();
      }
    } catch (IOException e) {
      e.printStackTrace();
    } finally {
      if (server != null) {
        try {
          server.close();
        } catch (IOException e) {
          e.printStackTrace();
        }
      }
    }
  }

  private static class ClientHandler extends Thread {
    private final Socket socket;
    private final BufferedReader request;
    private final PrintWriter response;

    public ClientHandler(Socket socket) throws IOException {
      this.socket = socket;
      this.request = new BufferedReader(new InputStreamReader(socket.getInputStream()));
      this.response = new PrintWriter(socket.getOutputStream());
    }

    public void run() {

      System.out.println("New client connection (" + new Date() + ")");

      try {
        ArrayList<String> requestLines = readIncomingRequestFromClient();
        assertCorrectRequestFormat(requestLines);
        String messageBody = requestLines.get(3);
        int questionNumber = extractQuestionNumberFromMessage(messageBody);
        String questionAnswer = DBConnection.askQuestion(questionNumber);
        replyQuestionAnswerToClient(messageBody, questionAnswer);

      } catch (IOException e) {
        System.out.println(e.toString());

      } finally {
        closeConnection();
        System.out.println("Connection closed.\n");
      }
    }

    private ArrayList<String> readIncomingRequestFromClient() throws IOException {
      ArrayList<String> lines = new ArrayList<>();
      lines.add(request.readLine());
      while (request.ready()) {
        lines.add(request.readLine());
      }
      return lines;
    }

    private void assertCorrectRequestFormat(ArrayList<String> requestLines) {
      String[] firstLineParts = requestLines.get(0).split(" ");
      assert firstLineParts[0].equals("POST");
      assert firstLineParts[1].equals("/ORM-Lib");
      assert firstLineParts[2].equals("HTTP/1.1");
      assert requestLines.get(1).equals("Host: localhost");
      assert requestLines.get(2).isEmpty();
      assert requestLines.size() == 4;
    }

    private int extractQuestionNumberFromMessage(String messageBody) {
      return Integer.parseInt(messageBody.substring(messageBody.length() - 1));
    }

    private void replyQuestionAnswerToClient(String messageBody, String questionAnswer) {
      int fileLength = questionAnswer.length();
      response.println("HTTP/1.1 200 OK");
      response.println("Server: Java HTTP Server: 1.0");
      response.println("Date: " + new Date());
      response.println("Content-type: text/plain");
      response.println("Content-length: " + fileLength);
      response.println();
      response.println("Requested body is:");
      response.println(messageBody);
      response.println();
      response.println(questionAnswer);
      response.flush();
    }

    private void closeConnection() {
      try {
        if (request != null) {
          request.close();
        }
        if (response != null) {
          response.close();
        }
        socket.close();

      } catch (IOException e) {
        System.err.println("Error closing stream: " + e.getLocalizedMessage());
      }
    }
  }
}